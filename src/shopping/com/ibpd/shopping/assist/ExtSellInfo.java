package com.ibpd.shopping.assist;

import java.util.Date;

public class ExtSellInfo {

	private Long orderId;
	private String productName;
	private Integer number;
	private Float price_f;
	private Double price_d;
	private Float total_f;
	private Double total_d;
	private Long tenantId;
	private String tenantName;
	private Date startDate;
	private Date endDate;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Float getPrice_f() {
		return price_f;
	}
	public void setPrice_f(Float priceF) {
		price_f = priceF;
	}
	public Double getPrice_d() {
		return price_d;
	}
	public void setPrice_d(Double priceD) {
		price_d = priceD;
	}
	public Float getTotal_f() {
		return total_f;
	}
	public void setTotal_f(Float totalF) {
		total_f = totalF;
	}
	public Double getTotal_d() {
		return total_d;
	}
	public void setTotal_d(Double totalD) {
		total_d = totalD;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
