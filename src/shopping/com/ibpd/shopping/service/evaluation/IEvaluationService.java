package com.ibpd.shopping.service.evaluation;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.EvaluationEntity;

public interface IEvaluationService extends IBaseService<EvaluationEntity> {
	List<EvaluationEntity> getListByAccountId(Long accId,Long productId,Integer pageSize,Integer pageIndex);
}
