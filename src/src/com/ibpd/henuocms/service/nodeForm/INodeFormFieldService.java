package com.ibpd.henuocms.service.nodeForm;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.NodeFormFieldEntity;

public interface INodeFormFieldService extends IBaseService<NodeFormFieldEntity> {

}
 