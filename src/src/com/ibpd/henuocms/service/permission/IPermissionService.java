package com.ibpd.henuocms.service.permission;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.PermissionEntity;

public interface IPermissionService extends IBaseService<PermissionEntity> {
}
 