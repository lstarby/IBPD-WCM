package com.ibpd.henuocms.service.subSite;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;

public interface ISubSiteService extends IBaseService<SubSiteEntity> {
	Long getPageTemplateId(Long siteId,Integer type);
	PageTemplateEntity getPageTemplate(Long siteId,Integer type);
} 