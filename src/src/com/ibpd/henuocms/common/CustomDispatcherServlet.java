package com.ibpd.henuocms.common;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.SourceFilteringListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ConfigurableWebEnvironment;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FrameworkServlet;
/**
 * 自定义servlet拦截器
 * <b>最初的作用是用来动态加载外部挂载模块的servlet请求</b>
 * @author mg by qq:349070443
 *
 */
public class CustomDispatcherServlet extends DispatcherServlet {

	/**
	 * initialization started 
	 */
	private static final long serialVersionUID = -3295909660734816914L;

	protected ServletConfig getLocalServletConfig(){
		IbpdLogger.getLogger(this.getClass()).info("==从这里加载另外的servletConfig信息===");
		ServletConfig config=getServletConfig();
		return config;
	}
	public void reload(){
		WebApplicationContext wc=WebApplicationContextUtils.getRequiredWebApplicationContext(getLocalServletConfig().getServletContext());
		this.refresh();
	}
	@Override
	protected void configureAndRefreshWebApplicationContext(
			ConfigurableWebApplicationContext wac) {
		IbpdLogger.getLogger(this.getClass()).info("==自定义的dispatcherServlet===");
		super.configureAndRefreshWebApplicationContext(wac);
	}

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		
		IbpdLogger.getLogger(this.getClass()).info("=-=-=-=-=-=-service=-=--=-=-=-=");
		if(arg0.getParameter("isrefreshConfig")!=null){
			IbpdLogger.getLogger(this.getClass()).info("=-=-=-=-=-=重新初始化=-=--=-=-=-=");
			arg1.getWriter().write("=重新初始化=");
			refresh();
		}
		super.service(arg0, arg1);
		
	}
	@Override
	protected void initFrameworkServlet() throws ServletException {
		// TODO Auto-generated method stub
		super.initFrameworkServlet();
	}

	@Override
	protected WebApplicationContext initWebApplicationContext() {
		// TODO Auto-generated method stub
		return super.initWebApplicationContext();
	}

}