<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
        <table>
				<tr>
					<td class="tit">
						<span>${cNameMap.siteOpenRule}</span>
					</td>
					<td class="val">
						<span>
							<select conf="true" name="siteOpenRule">
								<option id="timmingOpen">一直开启</option>
								<option id="timmingOpenAndNoClose">定时开启-永不关闭</option>
								<option id="timmingOpenAndTimmingClose">定时开启-定时关闭</option>
								<option id="timmingClose">定时关闭</option>
							</select>
						</span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteOpenRule}</span>
					</td>
				</tr>
			
				<tr>
					<td class="tit">
						<span>${cNameMap.siteOpenType}</span>
					</td>
					<td class="val">
						<span>
							<select conf="true" name="siteOpenType" onchange="swap(this)">
								<option id="everyday">每天</option>
								<option id="everyweek">每周</option>
								<option id="everymonth">每月</option>
								<option id="everyyear">每年</option>
							</select>
						</span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteOpenType}</span>
					</td>
				</tr>
			
        		<tr>
					<td class="tit">
						<span>${cNameMap.siteOpenTime}</span>
					</td>
					<td class="val">
						<span id="weeks"><input type="checkbox" id="week_1"/>星期一&nbsp;&nbsp;<input type="checkbox" id="week_2"/>星期二&nbsp;&nbsp;<input type="checkbox" id="week_3"/>星期三&nbsp;&nbsp;<input type="checkbox" id="week_3"/>星期四&nbsp;&nbsp;<input type="checkbox" id="week_5"/>星期五&nbsp;&nbsp;<input type="checkbox" id="week_6"/>星期六&nbsp;&nbsp;<input type="checkbox" id="week_7"/>星期日</span>
						<span><input type="text" conf="true" id="siteOpenTime_val" name="siteOpenTime" value="${cValMap.siteOpenTime}"/></span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteOpenTime}</span>
					</td>
				</tr>
			
				<tr>
					<td class="tit">
						<span>${cNameMap.siteCloseTime}</span>
					</td>
					<td class="val">
						<span><input type="text" conf="true" id="siteCloseTime_val" name="siteCloseTime" value="${cValMap.siteCloseTime}"/></span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteCloseTime}</span>
					</td>
				</tr>
        
        </table>
